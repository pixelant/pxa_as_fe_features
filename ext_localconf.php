<?php
if (!defined('TYPO3_MODE')) {
	die ('Access denied.');
}

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
	'Pixelant.' . $_EXTKEY,
	'Features',
	array(
		'Features' => 'list, content, propose, complete',

	),
	// non-cacheable actions
	array(
		'Features' => 'list, content, propose, complete',

	)
);

?>
