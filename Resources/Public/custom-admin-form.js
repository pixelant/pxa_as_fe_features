// ########## custom-admin-form.js ###########

(function($) {
    'use strict';

    /**
     * Input fields (images and PDF)
     */

    // initial fields
    var imageInputs = document.getElementsByClassName('js__image-loading'),
        imageInputsLength = imageInputs.length,
        fileInputs = document.getElementsByClassName('js__file-loading'),
        fileInputsLength = fileInputs.length;

    function handleFileSelect(e) {
        var wholeBlock = e.target.parentNode.parentNode,
            itemsWrap = wholeBlock.parentNode,
            previewNode = wholeBlock.childNodes[1],
            file = e.target.files[0],  // FileList object - first item only
            fileType = e.target.dataset.fileType;

        if (file !== undefined) {
            var reader = new FileReader();

            // Closure to capture the file information
            reader.onloadend = (function(theFile) {
                return function(e) {

                    /**
                     * Render thumbnail
                    */

                    // Creating nodes
                    var div = document.createElement('div'), titleSpan = document.createElement('span'), removeBtnSpan = document.createElement('span');

                    // Checking file type and starting to construct 
                    // preview node
                    if (fileType === 'img') {
                        // Only process image files
                        // Double checking file type
                        if (!file.type.match('image.*')) {
                            console.log('IMAGE REQUIRED');
                            return;
                        }
                        div.setAttribute('class', 'images-preview-list__item');

                        var imgSpan = document.createElement('span');

                        imgSpan.setAttribute('class', 'images-preview-list__item-img');
                        // imgSpan.style.backgroundImage = 'url(' + e.target.result + ')';
                        titleSpan.setAttribute('class', 'images-preview-list__item-title');
                        
                        imgSpan.innerHTML = ['<img class="img img-responsive" src="', e.target.result, '" title="', escape(theFile.name), '"/>'].join('');

                        div.appendChild(imgSpan);

                    } else if (fileType === 'file') {
                        // Only process PDF files
                        // Double checking file type
                        if (!file.type.match('application/pdf')) {
                            console.log('PDF REQUIRED');
                            return;
                        }
                        div.setAttribute('class', 'files-preview-list__item');

                        var iconSpan = document.createElement('span');

                        iconSpan.setAttribute('class', 'files-preview-list__item-icon icons icons icon-file-text2');
                        titleSpan.setAttribute('class', 'files-preview-list__item-title');

                        div.appendChild(iconSpan);
                    }

                    titleSpan.innerHTML = escape(theFile.name);
                    removeBtnSpan.innerHTML = '<input type="button" class="admin-form__remove-item" value="✕" />';
                    removeBtnSpan.addEventListener('click', removeBlock, false);

                    div.appendChild(titleSpan);
                    div.appendChild(removeBtnSpan);

                    /**
                     * END OF BLOCK Render thumbnail
                    */

                    // clean up preview node while updating file
                    while (previewNode.firstChild) {
                        previewNode.removeChild(previewNode.firstChild)
                    }

                    // insert thumbnail
                    previewNode.insertBefore(div, null);
                    if (!hasClass(wholeBlock, '_updatable')) {
                        wholeBlock.className += ' _updatable';
                    }

                    // show next input field
                    var count = 0, visible = 0, active = 0;
                    for (var i = 0, len = itemsWrap.childNodes.length; i < len; i++) {
                        if (hasClass(itemsWrap.childNodes[i], 'admin-form__add-img') || hasClass(itemsWrap.childNodes[i], 'admin-form__add-file')) {
                            count++;
                            if (hasClass(itemsWrap.childNodes[i], '_visible')) {
                                visible++;
                            }
                            if (hasClass(itemsWrap.childNodes[i], '_updatable')) {
                                active++;
                            }
                        }
                    }
                    if (count > visible && active === visible) {
                        for (var i = 0, len = itemsWrap.childNodes.length; i < len; i++) {
                            if (!hasClass(itemsWrap.childNodes[i], '_visible')) {
                                itemsWrap.childNodes[i].className += ' _visible';
                                continue;
                            }
                        }
                    }
                };
            })(file);

            // Read in the image file as a data URL
            try {
                reader.readAsDataURL(file);
            } catch (err) {
                console.log(err);
            }
        } else {
            // fix for bug of canseling file changing.

            // clean up preview node
            while (previewNode.firstChild) {
                previewNode.removeChild(previewNode.firstChild)
            }
            wholeBlock.className = wholeBlock.className.replace(new RegExp('(?:^|\\s)'+ '_updatable' + '(?:\\s|$)'), '');
        
            // show only one input field
            var visible = 0, active = 0;
            for (var i = 0, len = itemsWrap.childNodes.length; i < len; i++) {
                if (itemsWrap.childNodes[i].nodeType === 1 && (hasClass(itemsWrap.childNodes[i], 'admin-form__add-img') || hasClass(itemsWrap.childNodes[i], 'admin-form__add-file'))) {
                    if (hasClass(itemsWrap.childNodes[i], '_visible')) {
                        visible++;
                    }
                    if (hasClass(itemsWrap.childNodes[i], '_updatable')) {
                        active++;
                    }
                }
            }
            if (active < visible - 1) {
                for (var i = itemsWrap.childNodes.length - 1; i >= 0; i--) {
                    if (itemsWrap.childNodes[i].nodeType === 1 && hasClass(itemsWrap.childNodes[i], '_visible') && !hasClass(itemsWrap.childNodes[i], '_updatable')) {
                        // hide extra fields
                        itemsWrap.childNodes[i].className = itemsWrap.childNodes[i].className.replace(new RegExp('(?:^|\\s)'+ '_visible' + '(?:\\s|$)'), '');
                        break;
                    }
                }
            }
        }


    }

    function removeBlock(e) {
        var wholeBlock = e.target.parentNode.parentNode.parentNode.parentNode,
            itemsWrap = wholeBlock.parentNode,
            inputFile = wholeBlock.childNodes[3].childNodes[5],
            previewNode = wholeBlock.childNodes[1];

        // clean up preview node
        while (previewNode.firstChild) {
            previewNode.removeChild(previewNode.firstChild)
        }

        // remove class from wholeBlock '_updatable'
        wholeBlock.className = wholeBlock.className.replace(new RegExp('(?:^|\\s)'+ '_updatable' + '(?:\\s|$)'), '');

        if (inputFile.value) {
            // for modern browsers
            try {
                inputFile.value = '';
                // inputFile.value = null;
            } catch (err) {
                console.log(err);
            }
            // for old ie: IE5 ~ IE10
            if (inputFile.value) {
                var clone = inputFile.cloneNode(false);
                clone.addEventListener('change', handleFileSelect, false);
                inputFile.parentNode.replaceChild(clone, inputFile);
            }
        }

        // show only one input field
        var visible = 0, active = 0;
        for (var i = 0, len = itemsWrap.childNodes.length; i < len; i++) {
            if (itemsWrap.childNodes[i].nodeType === 1 && (hasClass(itemsWrap.childNodes[i], 'admin-form__add-img') || hasClass(itemsWrap.childNodes[i], 'admin-form__add-file'))) {
                if (hasClass(itemsWrap.childNodes[i], '_visible')) {
                    visible++;
                }
                if (hasClass(itemsWrap.childNodes[i], '_updatable')) {
                    active++;
                }
            }
        }
        if (active < visible - 1) {
            for (var i = itemsWrap.childNodes.length - 1; i >= 0; i--) {
                if (itemsWrap.childNodes[i].nodeType === 1 && hasClass(itemsWrap.childNodes[i], '_visible') && !hasClass(itemsWrap.childNodes[i], '_updatable')) {
                    // hide extra fields
                    itemsWrap.childNodes[i].className = itemsWrap.childNodes[i].className.replace(new RegExp('(?:^|\\s)'+ '_visible' + '(?:\\s|$)'), '');
                    break;
                }
            }
        }
    }

    function hasClass(element, cls) {
        return (' ' + element.className + ' ').indexOf(' ' + cls + ' ') > -1;
    }

    // initial handlers
    for (var i = 0; i < imageInputsLength; i++) {
        imageInputs[i].addEventListener('change', handleFileSelect, false);
    }
    for (var i = 0; i < fileInputsLength; i++) {
        fileInputs[i].addEventListener('change', handleFileSelect, false);
    }

    /**
     * END OF BLOCK Input fields (images and PDF)
     */

    /**
     * DateTimePicker
     */

    $('.js__input-daterange_since').datetimepicker({
    	useCurrent: false,
    	showTodayButton: true,
    	showClear: true,
    	icons: {
    		time: 'icons icon-access_time',
            date: 'icons icon-format_list_bulleted',
            up: 'icons icon-arrow-up',
            down: 'icons icon-arrow-down',
            previous: 'icons icon-arrow-left',
            next: 'icons icon-arrow-right',
            today: 'icons icon-location_on',
            clear: 'icons icon-create',
            close: 'icons icon-clear'
    	},
    });
    $('.js__input-daterange_to').datetimepicker({
    	useCurrent: false,
    	showTodayButton: true,
    	showClear: true,
    	icons: {
    		time: 'icons icon-access_time',
            date: 'icons icon-format_list_bulleted',
            up: 'icons icon-arrow-up',
            down: 'icons icon-arrow-down',
            previous: 'icons icon-arrow-left',
            next: 'icons icon-arrow-right',
            today: 'icons icon-location_on',
            clear: 'icons icon-create',
            close: 'icons icon-clear'
    	},
    });

    $('.js__input-daterange_since').on('dp.hide', function(e) {
    	if ($(this).val() !== '') {
    		$('.js__input-daterange_to').data('DateTimePicker').minDate(e.date);

    		var dateVal = $('.js__input-daterange_to').val();

    		if (dateVal != '') {
    			var date1 = e.date._d;
	    		var date2 = new Date(dateVal);

	    		if (date1 > date2) {
	    			$('.js__input-daterange_to').val($(this).val());
	    		}
    		}
    	} else {
    		// If it's cleared or just closed after selecting
    		// without chosing date
    		$('.js__input-daterange_to').data('DateTimePicker').minDate(false);
    		$('.js__input-daterange_to').val('');
    	}
        
    });

    /**
     * END OF BLOCK DateTimePicker
     */

    /**
     * Validator
     */

    $.fn.validator.Constructor.FOCUS_OFFSET = 220;
    $('.js__admin-form').validator().on('submit', function(e) {
        if (e.isDefaultPrevented()) {
            // Handling invalid form.
            // Adding invalid class to each form 
            // that has empty required inputs.
            $(this).find('.js__admin-form__tab-pane').each(function() {
                // tab
                var $tab = $(this);
                var $refTab = $('.js__admin-form__nav-tabs').find('[href="#' + $tab.attr('id') + '"]');

                if ($tab.find('.has-error').length) {
                    $tab.addClass('_not-valid-tab');
                    $refTab.addClass('_not-valid-tab');
                } else {
                    $tab.removeClass('_not-valid-tab');
                    $refTab.removeClass('_not-valid-tab');
                }
            });
        }
    });

    $('.js__admin-form__geneate-preview').on('click', function(e) {
        var $inputs = $('.js__admin-form').find('[data-rel][type="text"]');
        var $textareas = $('.js__admin-form').find('textarea[data-rel]');
        var $images = $('.js__admin-form').find('[data-rel="img"][type="file"]');
        var $checkboxesRegion = $('.js__admin-form').find('[data-rel="region"][type="checkbox"]:checked');
        var $checkboxesThematic = $('.js__admin-form').find('[data-rel="thematic"][type="checkbox"]:checked');

        var imagesLength = $images.length;
        var $imagesWrp = $('.js__admin-form__preview_images');
        var $categoriesWrp = $('.js__admin-form__preview_categories');

        $imagesWrp.empty();
        $categoriesWrp.empty();

        $inputs.each(function() {
            var $input = $(this);
            var val = $input.val();
            var rel = $input.data('rel');

            $('.js__admin-form__preview_' + rel).text(val);
        });

        $textareas.each(function() {
            var $input = $(this);
            // var val = $input.val();
            var val = $input.val().replace(/\n/gi, '<br>');
            var rel = $input.data('rel');

            // $('.js__admin-form__preview_' + rel).text(val);
            $('.js__admin-form__preview_' + rel).html(val);
        });
        $images.each(function() {
            var $input = $(this);
            var val = $input.val();
            var rel = $input.data('rel');

            if (this.files && this.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    var $img = $('<img />', {
                        src: e.target.result
                    }).appendTo($imagesWrp).wrap('<a href="#"></a>');
                }

                reader.readAsDataURL(this.files[0]);
            }
        });
        $checkboxesRegion.each(function() {
            var $input = $(this);
            var val = $input.data('title');
            var rel = $input.data('rel');

            var $category = $('<span />', {
                class: 'news-list-category',
                text: val
            }).appendTo($categoriesWrp);
        });
        $checkboxesThematic.each(function() {
            var $input = $(this);
            var val = $input.data('title');
            var rel = $input.data('rel');

            var $category = $('<span />', {
                class: 'news-list-category',
                text: val
            }).appendTo($categoriesWrp);
        });
    });

    

    /**
     * END OF BLOCK Validator
     */

})(jQuery);

// ^^^^^^^^^^ custom-admin-form.js ^^^^^^^^^^^
