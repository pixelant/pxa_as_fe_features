<?php

$EM_CONF[$_EXTKEY] = array(
	'title' => 'AS frontend features',
	'description' => 'edit microsite content and sufggest news/events',
	'category' => 'plugin',
	'author' => 'Andrii Pozdieiev',
	'author_email' => 'andriy.p@pixelant.se',
	'author_company' => '',
	'shy' => '',
	'priority' => '',
	'module' => '',
	'state' => 'beta',
	'internal' => '',
	'createDirs' => '',
	'modify_tables' => '',
	'clearCacheOnLoad' => 0,
	'lockType' => '',
	'version' => '0.3',
	'constraints' => array(
		'depends' => array(
			'extbase' => '6.0',
			'fluid' => '6.0',
			'typo3' => '6.0',
		),
		'conflicts' => array(
		),
		'suggests' => array(
		),
	),
);

?>
