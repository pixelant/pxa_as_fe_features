<?php
namespace Pixelant\PxaAsFeFeatures\Controller;

use \TYPO3\CMS\Core\Utility\GeneralUtility;
use \TYPO3\CMS\Extbase\Utility\LocalizationUtility;

class FeaturesController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController {

    const NEWS = 0;
    const EVENT = 1;

    /**
	 * newsRepository
	 *
	 * @var \GeorgRinger\News\Domain\Repository\NewsRepository
	 * @inject
	 */
	protected $newsRepository;

	/**
	 * action list
	 *
	 * @return void
	 */
	public function listAction() {
        
		//list of sites where user can edit content (if any) according to his usergroups
		if (count($this->settings['adminContentEditUid']) > 0){
			$counter = 0;
			foreach ($this->settings['adminContentEditUid'] as $uid){
				$pid = $GLOBALS['TYPO3_DB']->exec_SELECTgetSingleRow(
					'pid',
					'tt_content',
					'deleted=0 AND uid='.$uid
					);
				$page = $GLOBALS['TYPO3_DB']->exec_SELECTgetSingleRow(
					'title',
					'pages',
					'deleted=0 AND uid='.$pid['pid']
					);
				if (is_array($page)){
					$contents[$counter]['uid'] = $uid;
					$contents[$counter]['pid'] = $pid['pid'];
					$contents[$counter]['title'] = $page['title'];
					$counter++;
				}
			}
		}
		if (count($contents) > 0){
			$this->view->assign('contents', $contents);
		}
		
		if (count($this->settings['adminSiteCategory']) > 0){
		    $this->view->assign('proposeLink', 1);
		}

	}

	/**
	 * action content
	 *
	 * @return void
	 */
	public function contentAction() {
		//get content uid
		$args = $this->request->getArguments();
		$uid = $args['uid'];
		//check if current uid is valid for user
		$validContent = false;
		if (count($this->settings['adminContentEditUid']) > 0){
			foreach ($this->settings['adminContentEditUid'] as $content){
				if ($content == $uid){
					$validContent = true;
				}
			}
		}
		if (!$validContent){
			echo 'something is wrong...';
		} else {
			
			//check if we don't have changes to save
			$data = GeneralUtility::_GP('edit_content');
			if (is_array($data)){
				$fieldValues = array(
					'header' => $data['header'],
					'bodytext' => $data['bodytext']
				);
				$GLOBALS['TYPO3_DB']->exec_UPDATEquery(
					'tt_content',
					'uid='.$data['uid'],
					$fieldValues
					);
					
				//clear cache for the page where edited content is placed
				$tce = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\\CMS\\Core\\DataHandling\\DataHandler');
				$tce->clear_cacheCmd($data['pid']);
				
				$this->addFlashMessage(
				   LocalizationUtility::translate('message_content', 'pxa_as_fe_features'),
				   LocalizationUtility::translate('message_content_title', 'pxa_as_fe_features'),
				   \TYPO3\CMS\Core\Messaging\FlashMessage::OK
				);

			}
			
			$content = $GLOBALS['TYPO3_DB']->exec_SELECTgetSingleRow(
				'uid, pid, header, bodytext',
				'tt_content',
				'deleted=0 AND uid='.$uid
				);

			$this->view->assign('content', $content);
			
			$page = $GLOBALS['TYPO3_DB']->exec_SELECTgetSingleRow(
					'title',
					'pages',
					'deleted=0 AND uid='.$content['pid']
				);
			$this->view->assign('page', $page['title']);
		}
	
	}
	
	/**
	 * action content
	 *
	 * @return void
	 */
	public function proposeAction() {
		//get content uid
		$args = $this->request->getArguments();
		$type = $args['type'];
		$this->view->assign('type', $type);
		$this->view->assign('user', $GLOBALS['TSFE']->fe_user->user);
		$showSubsites = false;
		$allSubsites = false;
		foreach($this->settings['adminSiteCategory'] as $subsite){
			if ($subsite == 'all'){
				$allSubsites = true;
				$showSubsites = true;
				break;
			} 
		}
		if ($allSubsites){
			$subsites = $GLOBALS['TYPO3_DB']->exec_SELECTquery(
				'uid, title',
				'sys_category',
				'deleted=0 AND parent='.$this->settings['subsitesCategory']
				);
			$counter = 0;	
			foreach($subsites as $subsite){
				$subsiteCategory[$counter]['id'] = $subsite['uid'];
				$subsiteCategory[$counter]['title'] = $subsite['title'];
				$counter++;
			}
		} else {
			if (count($this->settings['adminSiteCategory']) > 1){
				$showSubsites = true;
			}
			$counter = 0;
			foreach($this->settings['adminSiteCategory'] as $subsite){
				$subsiteInfo = $GLOBALS['TYPO3_DB']->exec_SELECTgetSingleRow(
					'uid, title',
					'sys_category',
					'deleted=0 AND uid='.$subsite
					);
				$subsiteCategory[$counter]['id'] = $subsiteInfo['uid'];
				$subsiteCategory[$counter]['title'] = $subsiteInfo['title'];
				$counter++;
			} 
		}
		
		$regions = $GLOBALS['TYPO3_DB']->exec_SELECTquery(
				'uid, title',
				'sys_category',
				'deleted=0 AND parent='.$this->settings['regionesCategory']
				);
		$counter = 0;	
		foreach($regions as $region){
			$regionsCategory[$counter]['id'] = $region['uid'];
			$regionsCategory[$counter]['title'] = $region['title'];
			$counter++;
		}
		$themes = $GLOBALS['TYPO3_DB']->exec_SELECTquery(
			'uid, title',
			'sys_category',
			'deleted=0 AND parent='.$this->settings['thematicCategory']
			);
		$counter = 0;	
		foreach($themes as $theme){
			$thematicCategory[$counter]['id'] = $theme['uid'];
			$thematicCategory[$counter]['title'] = $theme['title'];
			$counter++;
		}
		
		if ($this->settings['maxFilesUpload'] > 0){
		    $filecount = $this->settings['maxFilesUpload'];
		} else {
		    $filecount = 2;
		}
		$counter = 1;
		while ($counter <= $filecount){
		    $fileinputs[] = $counter;
		    $counter++;
		}
		
		$this->view->assign('showSubsites', $showSubsites);
		$this->view->assign('subsites', $subsiteCategory);
		$this->view->assign('regions', $regionsCategory);
		$this->view->assign('thematics', $thematicCategory);
		$this->view->assign('fileinputs', $fileinputs);
		
    }
    
    /**
	 * action complete
	 *
	 * @return void
	 */
	public function completeAction() {
		/** @var $objectManager \TYPO3\CMS\Extbase\Object\ObjectManager */
        $objectManager = GeneralUtility::makeInstance('TYPO3\\CMS\\Extbase\\Object\\ObjectManager');

        /** @var $categoryRepository \GeorgRinger\News\Domain\Repository\CategoryRepository */
        $categoryRepository = $objectManager->get('GeorgRinger\\News\\Domain\\Repository\\CategoryRepository');
        
        $persistenceManager = $objectManager->get("TYPO3\CMS\Extbase\Persistence\PersistenceManagerInterface");
		$storageRepository = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance("TYPO3\CMS\Core\Resource\StorageRepository");
		$storage = $storageRepository->findByUid(1);
        
		$args = $this->request->getArguments();
		$data = GeneralUtility::_GP('propose');
		if (null != $data){
            $item = new \GeorgRinger\News\Domain\Model\News;
            
            $item->setIsEvent($data['type']);
            $item->setTitle($data['title']);
            $item->setDatetime(strtotime($data['date_start']));
            $item->setAuthor($data['author_name']);
            $item->setAuthorEmail($data['author_email']);
            $item->setTeaser($data['teaser']);
            $item->setBodytext($data['description']);
            
            //additional fields for events
            if($data['type'] == 1){
                $item->setLocationSimple($data['location']);
                $date_source = strtotime($data['date_end']);
                $timestamp = date('Y-m-d H:i:s', $date_source);
                $item->seteventEnd(\DateTime::createFromFormat('Y-m-d H:i:s', $timestamp));
            }
            
            //categories
            foreach($data['category'] as $cat){
                foreach($cat as $k => $c){
                    if ($k == $c){
                        $catObj = $categoryRepository->findByUid($k);
                        $item->addCategory($catObj);
                    }
                }
            }
            
            //links
            trim($data['links']);
            if ($data['links'] != ''){
                $links = explode(PHP_EOL, $data['links']);
            } else {
                $links = false;
            }
            if ($links){
                foreach($links as $link){
                    $relatedLink = $objectManager->get('GeorgRinger\\News\\Domain\\Model\\Link');
                    $relatedLink->setUri($link);
                    $item->addRelatedLink($relatedLink);
                }
            }

            //images
		    $images = $this->prepareFiles($args['images'], true, $this->settings['imagesFolder']);
		    $countImg = 1;
		    foreach ($images as $image){
		        if (file_exists($image['path'] . $image['file'])) {
		            $imageFile = $storage->getFile($image['file']);
		            $media = $this->objectManager->get('GeorgRinger\\News\\Domain\\Model\\FileReference');
		            $media->setFileUid($imageFile->getUid());
		            $media->setPid($this->settings['storagePid']);
		            if($countImg == 1){
		                $media->setShowinpreview(1);
		            }
		            $item->addFalMedia($media);
		            $countImg++;
		        }
		    }
		    
		    //files
		    $files = $this->prepareFiles($args['files'], false, $this->settings['filesFolder']);
            foreach ($files as $file){
		        if (file_exists($file['path'] . $file['file'])) {
		            $relFile = $storage->getFile($file['file']);
		            $relatedFile = $this->objectManager->get('GeorgRinger\\News\\Domain\\Model\\FileReference');
		            $relatedFile->setFileUid($relFile->getUid());
		            $relatedFile->setPid($this->settings['storagePid']);
		            $item->addFalRelatedFile($relatedFile);
		        }
		    }

            $item->setPid($this->settings['storagePid']);
            
            if ($this->settings['moderation'] == 1){
                $item->setHidden(1);
            }
            
            $this->newsRepository->add($item);
            $persistenceManager->persistAll();
            
            //if admin moderation is enable we will send a mail to admin!
	        if ($this->settings['moderation'] == 1){
                if ($data['type'] == 1){
                    $typeWord = LocalizationUtility::translate('event_word', 'pxa_as_fe_features');
                } else {
                    $typeWord .= LocalizationUtility::translate('news_word', 'pxa_as_fe_features');
                }
                $subject = LocalizationUtility::translate('mail_subject1', 'pxa_as_fe_features') . ' ' . $typeWord . ' ' . LocalizationUtility::translate('mail_subject2', 'pxa_as_fe_features');
                
				$templatePath = $this->settings['templatePath'];
				if (strpos($templatePath, "EXT:") === 0){
					$templatePath = "typo3conf/ext/" . substr($templatePath, 4);
				}
				
                $mailView = $objectManager->get('TYPO3\\CMS\\Fluid\\View\\StandaloneView');
                $mailView->setTemplatePathAndFilename($templatePath."Features/Mail.html");
                $mailView->assign('item', $item);
                $mailView->assign('type', $typeWord);
                $message = $mailView->render();
                $mail = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\\CMS\\Core\\Mail\\MailMessage');
                $mail->setFrom(array($this->settings['mailSender'] => $this->settings['mailSenderName']))
		            ->setTo(array($this->settings['mailReceiver']))
		            ->setSubject($subject)
		            ->setBody($message, 'text/html')
		            ->send();
                }
            
            unset($_POST);
            header('Location:'.$_SERVER['REQUEST_URI'] . '&done=1');

		}
		if (GeneralUtility::_GP('done') == 1){
			$this->addFlashMessage(
				LocalizationUtility::translate('message_propose', 'pxa_as_fe_features'),
				LocalizationUtility::translate('message_propose_title', 'pxa_as_fe_features'),
				\TYPO3\CMS\Core\Messaging\FlashMessage::OK
			);
		}
	}

    /**
    * Uploading files from form and add random part to name
    */
    public function prepareFiles($files, $addRandom, $folder) {
        foreach ($files as $key => $file){
		    if (isset($file['tmp_name']) && $file['tmp_name'] != '' && $file['name'] != ''){
		        if ($addRandom){
		            $info = pathinfo($file['name']);
                    $no_extension =  basename($file['name'], '.' . $info['extension']);
		            $filenames[$key]['file'] = $no_extension . '_' . $this->generateRandomString() . '.' . $info['extension'];
		        } else {
		            $filenames[$key]['file'] = $file['name'];
		        }
		        $filenames[$key]['file'] = '/user_upload/' . $folder . '/' . $filenames[$key]['file'];
		        $filenames[$key]['path'] = PATH_site . 'fileadmin';
		        $target =  $filenames[$key]['path'] . $filenames[$key]['file'];
		        move_uploaded_file($file['tmp_name'], $target);
		    }
		}
		return $filenames;
    }

    /**
    * Add some random part to filename so there will be no file overwrite 
    * if user will upload files with same name 
    */
    public function generateRandomString($length = 6) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyz';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}

}
